
Description
-----------
The Content Type Filter lets you filter content listing pages by content type - In Admin


Resources
---------
You can read the module author's collection of articles on following site

http://digcms.com



Installation
------------
To install, copy the content_type_filter directory and all its contents to your modules
directory.


Configuration
-------------
There is no configuration yet we created.


API
---
No API yet


Authors
-------
Purab Kharat      (http://digcms.com)

If you use this module, find it useful, and want to send the authors a thank
you note, then use the Feedback/Contact page at the URLs above.

The authors can also be contacted for paid customizations of this and other
modules.
